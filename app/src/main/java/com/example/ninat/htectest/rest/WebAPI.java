package com.example.ninat.htectest.rest;

import com.example.ninat.htectest.model.Item;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by ninat on 30/05/2017.
 */


public interface WebAPI {


    @GET("danieloskarsson/mobile-coding-exercise/master/items.json")
    Observable<List<Item>> getData();


}
