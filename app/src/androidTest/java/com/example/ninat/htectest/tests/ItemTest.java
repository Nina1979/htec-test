package com.example.ninat.htectest.tests;

import android.support.test.runner.AndroidJUnit4;

import com.example.ninat.htectest.model.Item;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by ninat on 25/06/2017.
 */
@RunWith(AndroidJUnit4.class)
public final class ItemTest {

     @Test
    public void testMockItem(){
         //create a mock
          Item item = mock(Item.class);

         //define return value for method getTitle()
         when(item.getTitle()).thenReturn("Mockito");

         //use mock in test
         assertEquals(item.getTitle(),"Mockito");

     }

}
