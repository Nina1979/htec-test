package com.example.ninat.htectest.dagger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ninat on 13/06/2017.
 */
@Module
public class NetModule {
    public static String BASE_URL = "https://raw.githubusercontent.com/";
    public static Retrofit retrofit = null;

    public NetModule() {

    }

    @Provides
    @Singleton
    public Retrofit provideApiCient(){
           if (retrofit == null) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build();
            }
            return retrofit;
    }







}
