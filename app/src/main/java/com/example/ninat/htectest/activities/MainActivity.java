package com.example.ninat.htectest.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ninat.htectest.R;
import com.example.ninat.htectest.adapter.ListAdapter;
import com.example.ninat.htectest.dagger.DaggerNetComponent;
import com.example.ninat.htectest.dagger.NetModule;
import com.example.ninat.htectest.model.Item;
import com.example.ninat.htectest.rest.WebAPI;
import com.orm.SugarRecord;
import com.orm.query.Select;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;


public class MainActivity extends AppCompatActivity {
    ListAdapter mAdapter;
    Observable<List<Item>> mObservable;
    @BindView(R.id.itemList)
    ListView lvBlogList;
    @Inject
    Retrofit retrofit;
    @BindView(R.id.loader)
    TextView txtLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Butterknife bind
        ButterKnife.bind(this);

        //Checks if internet is turned on and if it isn't toasts the message to the user.
        if (!isNetworkAvailable()) {
            Toast.makeText(MainActivity.this, "Turn your Internet connection on!", Toast.LENGTH_LONG).show();
            return;
        }

        // Build injectable modules
        DaggerNetComponent.builder().netModule(new NetModule()).build().inject(this);


        lvBlogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DisplayItemActivity.class);
                Item item = mAdapter.getItem(position);
                intent.putExtra("ItemId", item.getId());
                MainActivity.this.startActivity(intent);
                mAdapter.notifyDataSetChanged();
            }
        });

        //Gets cached date when list of items were saved in SharedPreference
        Long mCacheDate = PreferenceManager.getDefaultSharedPreferences(MainActivity.this)
                .getLong("DateOfSavingItemListToDatabase", 0);

        //Checks if cache exists
        if (mCacheDate != 0) {
            //checks if data is not expired(its exired if it is older than 6 hours)
            if (System.currentTimeMillis() - 21600000L <= mCacheDate) {
                //If true it loads list of items, creates an instance of mAdapter and sets this mAdapter on listview
                List<Item> mItems = Select.from(Item.class).list();
                if (!mItems.isEmpty()) {
                    mAdapter = new ListAdapter(MainActivity.this, mItems);
                    lvBlogList.setAdapter(mAdapter);
                    return;
                }
            } else {
                //if cache is expired delete all data from database
                SugarRecord.deleteAll(Item.class);
            }

        }
        // Sends new request to server and returns list of items
        getItem();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //unsubscribe the observable if it exists
        if (mObservable != null) {
            mObservable.unsubscribeOn(Schedulers.newThread());
        }
    }


    public void getItem() {

        this.txtLoader.setVisibility(View.VISIBLE);
        //Sends a call to a server
        WebAPI api = retrofit.create(WebAPI.class);

        mObservable = api.getData()
                .doOnNext(new Consumer<List<Item>>() {
                    @Override
                    public void accept(@NonNull List<Item> items) throws Exception {
                        MainActivity.this.saveItem(items);
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        mObservable.subscribe(new Observer<List<Item>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<Item> items) {
                mAdapter = new ListAdapter(MainActivity.this, items);
                lvBlogList.setAdapter(mAdapter);
                MainActivity.this.txtLoader.setVisibility(View.GONE);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

    }

    private void saveItem(List<Item> items) {
        //Saves items to database
        for (Item i : items) {
            i.save();
        }
        //Saves exact time when items were saved into database
        Date date = new Date(System.currentTimeMillis());
        PreferenceManager.getDefaultSharedPreferences(MainActivity.this)
                .edit()
                .putLong("DateOfSavingItemListToDatabase", date.getTime())
                .apply();

    }

    //Checks if the internet is turned on
    private boolean isNetworkAvailable() {
        ConnectivityManager mConnectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
