package com.example.ninat.htectest.tests;

import android.support.test.runner.AndroidJUnit4;

import com.example.ninat.htectest.model.Item;
import com.example.ninat.htectest.rest.WebAPI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

/**
 * Created by ninat on 27/06/2017.
 */
@RunWith(AndroidJUnit4.class)
public class ItemMockAdapterTest {


    private MockRetrofit mockRetrofit;
    private Retrofit retrofit;
    public Observable<List<Item>> observable;

    @Before
    public void setUp() throws Exception {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://test.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        NetworkBehavior behavior = NetworkBehavior.create();

        mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build();
    }

    @Test
    public void testRandomItemRetrieval() throws Exception {
        BehaviorDelegate<WebAPI> delegate = mockRetrofit.create(WebAPI.class);
        WebAPI mockWebApi = new MockWebApi(delegate);


        //Actual Test
        observable = mockWebApi.getData();
        Item it = (Item) observable.blockingFirst();


        //Asserting response
        Assert.assertEquals("Test title", it.getTitle());
    }
}
