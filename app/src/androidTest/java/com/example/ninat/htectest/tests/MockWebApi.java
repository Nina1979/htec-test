package com.example.ninat.htectest.tests;

import com.example.ninat.htectest.model.Item;
import com.example.ninat.htectest.rest.WebAPI;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.mock.BehaviorDelegate;
/**
 * Created by ninat on 27/06/2017.
 */

public class MockWebApi implements WebAPI {

    private final BehaviorDelegate<WebAPI> delegate;

    public MockWebApi(BehaviorDelegate<WebAPI> delegate) {
        this.delegate = delegate;
    }

    @Override
    public Observable<List<Item>> getData() {
        Item item = new Item();
        item.setTitle("Test title");
        return delegate.returningResponse(item).getData();
    }
}
