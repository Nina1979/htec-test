package com.example.ninat.htectest.tests;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import com.example.ninat.htectest.R;
import com.example.ninat.htectest.activities.DisplayItemActivity;
import com.example.ninat.htectest.activities.MainActivity;
import com.example.ninat.htectest.model.Item;


import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by ninat on 26/06/2017.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SecondActivityTest {
    private Item item;

    @Before
    public void setUp() throws Exception {
        item = new Item();
        item.setTitle("Test title");
        item.setDescription("Test description");
        item.setImageURL("http://blank.com/image.jpg");
        item.save();
    }

    @Rule
    public ActivityTestRule<DisplayItemActivity> rule  = new  ActivityTestRule<>(DisplayItemActivity.class,true, false);

    @Test
    public void ensureIntentDataIsDisplayed() throws Exception {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.putExtra("ItemId", item.getId());
        rule.launchActivity(intent);

        DisplayItemActivity activity = rule.getActivity();

        View txtTitle = activity.findViewById(R.id.txtTitleName);

        assertThat(txtTitle ,notNullValue());
        assertThat(txtTitle, instanceOf(TextView.class));
        TextView textView = (TextView) txtTitle;
        assertThat(textView.getText().toString(),is(item.getTitle()));
    }
}