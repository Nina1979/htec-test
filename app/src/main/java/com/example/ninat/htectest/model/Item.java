package com.example.ninat.htectest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;



/**
 * Created by ninat on 30/05/2017.
 */
public class Item extends SugarRecord implements Serializable {
    @SerializedName("image")
    @Expose
    private String imageURL;
    @Expose
    private String description;
    @Expose
    private String title;

    public Item() {    }

    public String getTitle() {return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getImageURL() {return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

}
