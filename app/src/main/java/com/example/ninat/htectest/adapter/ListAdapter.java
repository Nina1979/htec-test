package com.example.ninat.htectest.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ninat.htectest.R;
import com.example.ninat.htectest.model.Item;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ninat on 30/05/2017.
 */

/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public class ListAdapter extends ArrayAdapter {
    private Context mContext;
    List<Item> mItems;


    public ListAdapter(Context context, List<Item> items) {

        super(context, 0, items);
        this.mItems = items;
        this.mContext = context;

    }

    //saves accesses to views
    static class ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtDescription)
        TextView txtDescription;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public Item getItem(int position){
        return this.mItems.get(position);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Gets the data item for this position
        ViewHolder viewHolder;
        final Item item = (Item) getItem(position);


        // Checks if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object

        if (item != null) {
            viewHolder.txtTitle.setText(item.getTitle());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (item != null) {
                viewHolder.txtDescription.setText(Html.fromHtml(item.getDescription(), Html.FROM_HTML_MODE_COMPACT));
                viewHolder.txtDescription.setEllipsize(TextUtils.TruncateAt.END);
                viewHolder.txtDescription.setSingleLine(false);
                int n = 2;
                viewHolder.txtDescription.setLines(n);
            }
        } else if (item != null) {
            viewHolder.txtDescription.setText(Html.fromHtml(item.getDescription()));
        }

        // Return the completed view to render on screen
        return convertView;
    }
}



