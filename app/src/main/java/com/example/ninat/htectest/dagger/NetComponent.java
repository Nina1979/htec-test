package com.example.ninat.htectest.dagger;

import com.example.ninat.htectest.activities.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ninat on 13/06/2017.
 */
@Singleton
@Component(modules = {NetModule.class})
public interface NetComponent {
    void inject(MainActivity mainActivity);
}
