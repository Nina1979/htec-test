package com.example.ninat.htectest.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ninat.htectest.R;
import com.example.ninat.htectest.model.Item;
import com.orm.SugarRecord;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ninat on 30/05/2017.
 */

public class DisplayItemActivity extends AppCompatActivity {
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.txtDescription)
    TextView description;
    @BindView(R.id.txtTitleName)
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_item);
        ButterKnife.bind(this);

        //Takes message from intent by the key
        Bundle extras = getIntent().getExtras();
        Long itemID = extras.getLong("ItemId");

        //Checks if there is internet connection on
        if (!isNetworkAvailable()) {
            Toast.makeText(this, "Turn your Internet connection on!", Toast.LENGTH_LONG).show();
        }
        //Takes an item from database which have this itemID
        Item item = SugarRecord.findById(Item.class, itemID);

        if (item != null) {
            //if item exists, view is updated
            description.setText(item.getDescription());
            Glide.with(this).load(item.getImageURL()).into(image);
            title.setText(item.getTitle());
        }
    }

    //Checks if the internet is turned on
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

